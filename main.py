import RPi.GPIO as GPIO
from RPLCD.i2c import CharLCD
from time import sleep


class PlantWater():

    def __init__(self, gpio_board_num):
        self.gpio_board_num = gpio_board_num
        self.lcd = None
        self._setup()
    
    def _setup(self):
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(self.gpio_board_num, GPIO.OUT)
        self.set_output_low()
        self.lcd = self.init_lcd()
        
    def set_output_high(self):
        GPIO.output(self.gpio_board_num,1)

    def set_output_low(self):
        GPIO.output(self.gpio_board_num,0)


    def init_lcd(self):
        # init lcd
        lcd = CharLCD(i2c_expander='PCF8574', address=0x27, port=1,
                    cols=16, rows=2, dotsize=8,
                    charmap='A02',
                    auto_linebreaks=False,
                    backlight_enabled=True)
        return lcd
    
    def write_message(self, line: int = 0, message_string: str = 'example'):
        message_string = message_string[0:16]
        # clear line
        self.lcd.cursor_pos = (line, 0)
        self.lcd.write_string(" " * 15)
        self.lcd.cursor_pos = (line, 0)
        self.lcd.write_string(message_string)
        self.lcd._backlight
    
    def water_plant(self, duration: int = 5):
        self.set_output_high()
        sleep(duration)
        self.set_output_low()
    
    def set_backlight_off(self):
        self.lcd._set_backlight_enabled(False)
    
    def set_backlight_on(self):
        self.lcd._set_backlight_enabled(True)
    




if __name__ == "__main__":
    plant_waterer = PlantWater(11)